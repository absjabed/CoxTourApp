package com.jabed_parvez.coxtour;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

public class PeraActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pera);


        ListView peraListView;

        final ArrayList<JetshiItem> arrayList;
        BaseAdapter adapter;




        peraListView=(ListView) findViewById(R.id.peraListView);
        arrayList=new ArrayList<JetshiItem>();

        arrayList.add(new JetshiItem("শামসুর রহমান শিমুল ","ম্যানেজার \n" +
                "মোবাইল : ০১৮১৬২৭৭৩৪৪\n" +
                "মোবাইল : ০১৮২২৮৬৯৯৮৫"));
        arrayList.add(new JetshiItem("নুর মোহাম্মাদ","ম্যানেজার \n" +

                "মোবাইল : ০১৭৮৩৫৯৮৮৯২"));
        arrayList.add(new JetshiItem("শামসুর রহমান (শিমুল)",
                "ঠিকানা : দরিয়ানগর\n" +
                "মোবাইল : ০১৮৩৩৭৯৫১৫৮"));
        adapter=new BaseAdapter() {

            LayoutInflater inflater=(LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            @Override
            public View getView(int position, View view, ViewGroup viewGroup) {
                if (view==null) {
                    view=inflater.inflate(R.layout.jeski_list_item, null);
                }
                TextView nameTextView=(TextView) view.findViewById(R.id.jetskilistItemTextView);
                TextView mobileTextView=(TextView) view.findViewById(R.id.jetskimobileTextView);

                nameTextView.setText(arrayList.get(position).getJetname());

                mobileTextView.setText(arrayList.get(position).getMobilenumber());

                return view;
            }

            @Override
            public long getItemId(int position) {
                // TODO Auto-generated method stub
                return 0;
            }

            @Override
            public Object getItem(int position) {

                return arrayList.get(position);
            }

            @Override
            public int getCount() {
                // TODO Auto-generated method stub
                return arrayList.size();
            }
        };

        peraListView.setAdapter(adapter);


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
             /*   Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();*/
            }
        });
    }

}
