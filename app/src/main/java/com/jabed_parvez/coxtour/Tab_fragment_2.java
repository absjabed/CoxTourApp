package com.jabed_parvez.coxtour;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by PARVEZ on 09-Oct-16.
 */
public class Tab_fragment_2 extends Fragment {

    public View onCreateView(final LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

       return inflater.inflate(R.layout.tab_fragment_2, container, false);
    }
}
