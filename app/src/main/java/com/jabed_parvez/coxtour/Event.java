package com.jabed_parvez.coxtour;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;

public class Event extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        setContentView(R.layout.activity_event);

        GridView gridview = (GridView) findViewById(R.id.gridviewevent);

        gridview.setAdapter(new FruitAdapter(
                new FruitItem("Photography",R.drawable.photo),
                new FruitItem("Beach Bike",R.drawable.beachbikea),
                new FruitItem("Jet Ski",R.drawable.jet),
                new FruitItem("Para Seiling",R.drawable.pera),
                new FruitItem("Culture",R.drawable.culture)));


        gridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
                switch(position) {
                    case 0:
                        Intent intent = new Intent(Event.this,BlankActivity.class);
                        startActivity(intent);
                        break;
                    case 1:
                        Intent intent1 = new Intent(Event.this,BeachBike.class);
                        startActivity(intent1);
                        break;
                    case 2:
                        Intent intent2 = new Intent(Event.this,Jet2Activity.class);
                        startActivity(intent2);
                        break;
                    case 3:
                        Intent intent3 = new Intent(Event.this,PeraActivity.class);
                        startActivity(intent3);
                        break;
                    case 4:
                        Intent intent4 = new Intent(Event.this,BlankActivity.class);
                        startActivity(intent4);
                        break;
                }
            }
        });

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();*/
            }
        });
    }

}
