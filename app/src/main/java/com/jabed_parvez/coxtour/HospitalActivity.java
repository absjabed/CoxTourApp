package com.jabed_parvez.coxtour;

import android.content.Intent;
import android.content.res.TypedArray;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class HospitalActivity extends AppCompatActivity implements AdapterView.OnItemClickListener {


        String[] hospital_names= new String[20];
        TypedArray hospital_pics;

        String[] hospital_email;
        String[] hospital_mobile;

        List<RowItem> rowItems;
        ListView mylistview;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hospital);

        rowItems = new ArrayList<RowItem>();

        hospital_names = getResources().getStringArray(R.array.hospital_names);

/*

        hospital_names[0] = "A";
        hospital_names[1] = "B";
        hospital_names[2] = "C";
        hospital_names[3] = "D";
        hospital_names[4] = "E";
        hospital_names[5] = "F";
        hospital_names[6] = "G";
        hospital_names[7] = "M";
        hospital_names[8] = "K";
*/


        hospital_pics = getResources().obtainTypedArray(R.array.hospital_pics);

        hospital_email = getResources().getStringArray(R.array.hospital_email);

        hospital_mobile = getResources().getStringArray(R.array.hospital_mobile);

        for (int i = 0; i < hospital_names.length; i++) {

            RowItem item = new RowItem(hospital_names[i], hospital_pics.getResourceId(i,0), hospital_email[i], hospital_mobile[i]);
            rowItems.add(i,item);
        }

        mylistview = (ListView) findViewById(R.id.list);
        CustomAdapter adapter = new CustomAdapter(this, rowItems);
        mylistview.setAdapter(adapter);

        mylistview.setOnItemClickListener(this);

    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {


        String hospital_name = rowItems.get(position).getHospital_name();
        Toast.makeText(getApplicationContext(), "" + hospital_name,
                Toast.LENGTH_SHORT).show();
        if(position==5)
        {
            Intent intent = new Intent(HospitalActivity.this,BlankActivity.class);
            startActivity(intent);
        }
        if(position==0)
        {
            Intent intent = new Intent(HospitalActivity.this,BlankActivity.class);
            startActivity(intent);
        }

    }




}
