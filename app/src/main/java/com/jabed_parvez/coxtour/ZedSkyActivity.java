package com.jabed_parvez.coxtour;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ExpandableListView;

public class ZedSkyActivity extends AppCompatActivity implements ExpandableListView.OnChildClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.micks_pizza);

        final PizzaCatagory[] catagories = new PizzaCatagory[]{
                new PizzaCatagory("Fruit",
                        new PizzaTopping("Pineapple"),
                        new PizzaTopping("Avocado"),
                        new PizzaTopping("Apple")),
                new PizzaCatagory("Meat",
                        new PizzaTopping("Ham"),
                        new PizzaTopping("Bacon"),
                        new PizzaTopping("Beef Mince"),
                        new PizzaTopping("Ribs"),
                        new PizzaTopping("Lamb"),
                        new PizzaTopping("Pepperoni"),
                        new PizzaTopping("Chorizo")),
                new PizzaCatagory("Sauces",
                        new PizzaTopping("Tomato & Herb"),
                        new PizzaTopping("Sweet & Sour"),
                        new PizzaTopping("Fruit Chutney"),
                        new PizzaTopping("Barbecue"),
                        new PizzaTopping("Pesto"))
        };

        final ExpandableListView list =
                (ExpandableListView)findViewById(R.id.pizzas);

        list.setAdapter(new PizzaToppingsAdapter(catagories));
        list.setOnChildClickListener(this);



    }

    @Override
    public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
        return false;
    }
}
