package com.jabed_parvez.coxtour;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class AinsriActivity extends AppCompatActivity {
    BaseAdapter adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ainsri);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ListView ainshriListview;

        final ArrayList<JetshiItem> arrayList;

        ainshriListview=(ListView) findViewById(R.id.ainshriListview);
        arrayList=new ArrayList<JetshiItem>();

        arrayList.add(new JetshiItem("পুলিশ",
                "POLICE"));
        arrayList.add(new JetshiItem(" র\u200D্যাব  ",
                "RAB"));
        arrayList.add(new JetshiItem("ম্যাজিস্ট্রেট",
                "MAGISTRATE"));
        arrayList.add(new JetshiItem("ডি.সি. ",
                "D.C."));
        arrayList.add(new JetshiItem("এ.ডি.সি. ",
                "A.D.C"));
        arrayList.add(new JetshiItem("ফায়ার সার্ভিস ",
                "FIRE SERVICE "));
        arrayList.add(new JetshiItem("লাইফ গার্ড ",
                "LIFE GOURD "));


        adapter=new BaseAdapter() {

            LayoutInflater inflater=(LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            @Override
            public View getView(int position, View view, ViewGroup viewGroup) {
                if (view==null) {
                    view=inflater.inflate(R.layout.jeski_list_item, null);
                }
                TextView nameTextView=(TextView) view.findViewById(R.id.jetskilistItemTextView);
                TextView mobileTextView=(TextView) view.findViewById(R.id.jetskimobileTextView);

                nameTextView.setText(arrayList.get(position).getJetname());

                mobileTextView.setText(arrayList.get(position).getMobilenumber());

                return view;
            }

            @Override
            public long getItemId(int position) {
                // TODO Auto-generated method stub
                return 0;
            }

            @Override
            public Object getItem(int position) {

                return arrayList.get(position);
            }

            @Override
            public int getCount() {
                // TODO Auto-generated method stub
                return arrayList.size();
            }
        };

        ainshriListview.setAdapter(adapter);

        ainshriListview.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {

            @Override
            public boolean onItemLongClick(AdapterView<?> arg0, View view,
                                           int position, long ID) {


                Toast.makeText(getBaseContext(), "One Item Removed", Toast.LENGTH_SHORT).show();

                return false;
            }
        });
ainshriListview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        switch(position) {
            case 0:
                Intent intent = new Intent(AinsriActivity.this,BlankActivity.class);
                startActivity(intent);
                break;
            case 1:
                Intent intent1 = new Intent(AinsriActivity.this,BlankActivity.class);
                startActivity(intent1);
                break;
            case 2:
                Intent intent2 = new Intent(AinsriActivity.this,BlankActivity.class);
                startActivity(intent2);
                break;
            case 3:
                Intent intent3 = new Intent(AinsriActivity.this,BlankActivity.class);
                startActivity(intent3);
                break;
            case 5:
                Intent intent5 = new Intent(AinsriActivity.this,BlankActivity.class);
                startActivity(intent5);
                break;
            case 9:
                Intent intent9 = new Intent(AinsriActivity.this,BlankActivity.class);
                startActivity(intent9);
                break;
        }
    }
});

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               /* Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();*/
            }
        });
    }


}
