package com.jabed_parvez.coxtour;

import android.app.Activity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;

public class HotelMotelActivity extends Activity implements ExpandableListView.OnChildClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hotel_motel);

        final PizzaCatagory[] catagories = new PizzaCatagory[]{
                new PizzaCatagory("লংবীচ হোটেল লিমিটেড",
                        new PizzaTopping("১৪- কলাতলী রোড"),
                        new PizzaTopping("হোটেল - মোটেল জোন, কক্সবাজার"),
                        new PizzaTopping("ল্যান্ড ফোনঃ ০৩৪১-৫১৮৪৩-৬\n" +
                                "সেলফোনঃ ০১৭৩০৩৩৮৯০৭-৯\n" +
                                "০১৭১১৫৩৭৮৬৮"),
                new PizzaTopping("ইমেইলঃ info@longbeachhotelbd.com\n" +
                        "ফ্যাক্সঃ ০৩৪১-৫১০৬২")),
                new PizzaCatagory("সী-গাল হোটেল লিঃ",

                        new PizzaTopping("হোটেল - মোটেল জোন\n" +
                                "সী-বীচ, কক্সবাজার\n" +
                                "ল্যান্ড ফোনঃ ০৩৪১-৬২৪৮০-৯০\n" +
                                "সেলফোনঃ ০১৭৬৬-৬৬৬৫৩১\n" +
                                "ইমেইলঃ info@seagullhotelbd.com\n" +
                                "ফ্যাক্সঃ ০৩৪১-৬৪৪৩৬")),

        };

        final ExpandableListView list =
                (ExpandableListView)findViewById(R.id.pizzas);

        list.setAdapter(new PizzaToppingsAdapter(catagories));
        list.setOnChildClickListener(this);

    }

    @Override
    public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
        return false;
    }
}
