package com.jabed_parvez.coxtour;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;

public class EventfinalActivity extends AppCompatActivity implements AdapterView.OnItemClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        final LayoutInflater inflater = getLayoutInflater();

        final GridView view = (GridView)inflater.inflate(
                R.layout.activity_eventfinal,
                null);

        view.setAdapter(new FruitAdapter(
                new FruitItem("Photography",R.drawable.photo),
                new FruitItem("Beach Bike",R.drawable.beachbikea),
                new FruitItem("Jet Ski",R.drawable.jet),
                new FruitItem("Para Seiling",R.drawable.pera),
                new FruitItem("Culture",R.drawable.culture)));

        setContentView(view);

        view.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                switch(position) {
                    case 0:
                        Intent intent = new Intent(EventfinalActivity.this,ZedSkyActivity.class);
                        startActivity(intent);
                        break;
                    case 1:
                        Intent intent1 = new Intent(EventfinalActivity.this,ZedSkyActivity.class);
                        startActivity(intent1);
                        break;
                    case 2:
                        Intent intent2 = new Intent(EventfinalActivity.this,ZedSkyActivity.class);
                        startActivity(intent2);
                }
            }
        });
    }





    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

    }
}
