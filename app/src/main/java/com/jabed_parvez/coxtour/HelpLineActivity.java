package com.jabed_parvez.coxtour;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

public class HelpLineActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_help_line);


        ListView contListView;

        final ArrayList<JetshiItem> arrayList;
        BaseAdapter adapter;




        contListView=(ListView) findViewById(R.id.contListView);
        arrayList=new ArrayList<JetshiItem>();

        arrayList.add(new JetshiItem("হট লাইন ",
                "মোবাইল : ০১৭৬৯৬৯০৭৪০\n" +
                        "মোবাইল : ০১৭৬৯৬৯০৭৩৪\n" +
                "মোবাইল : ০১৭৬৯৬৯০৭৩২"));
        arrayList.add(new JetshiItem("সহকারি পুলিশ সুপার","টুরিস্ট পুলিশ \n" +
                "মোবাইল : ০১৭৬৯৬৯০৭৩৪"));
        arrayList.add(new JetshiItem("অতিরিক্ত পুলিশ সুপার","টুরিস্ট পুলিশ \n" +
                        "মোবাইল : ০১৭৬৯৬৯০৭৩২"));
        arrayList.add(new JetshiItem("সী সেইফ লাইফ গার্ড","\tজনাব ইমতিয়াজ আহমেদ\n" +
                "\tম্যানেজার \n" +
                "\tফোন- ০১৬৭৬-৮৪০৮০৪"));
        arrayList.add(new JetshiItem("রবি লাইফ গার্ড","জনাব সৈয়দ নুর\n" +
                "\tম্যানেজার\n" +
                "\tফোন- ০১৮১৮-৪৬৬৫৮৪"));
        arrayList.add(new JetshiItem("ইয়াছির লাইফ গার্ড","জনাব মোস্তফা কামাল \n" +
                "\tম্যানেজার\n" +
                "\tফোন- ০১৭১১-১০২৮২৯\n"));

        adapter=new BaseAdapter() {

            LayoutInflater inflater=(LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            @Override
            public View getView(int position, View view, ViewGroup viewGroup) {
                if (view==null) {
                    view=inflater.inflate(R.layout.jeski_list_item, null);
                }
                TextView nameTextView=(TextView) view.findViewById(R.id.jetskilistItemTextView);
                TextView mobileTextView=(TextView) view.findViewById(R.id.jetskimobileTextView);

                nameTextView.setText(arrayList.get(position).getJetname());

                mobileTextView.setText(arrayList.get(position).getMobilenumber());

                return view;
            }

            @Override
            public long getItemId(int position) {
                // TODO Auto-generated method stub
                return 0;
            }

            @Override
            public Object getItem(int position) {

                return arrayList.get(position);
            }

            @Override
            public int getCount() {
                // TODO Auto-generated method stub
                return arrayList.size();
            }
        };

        contListView.setAdapter(adapter);




        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               /* Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();*/
            }
        });
    }

}
