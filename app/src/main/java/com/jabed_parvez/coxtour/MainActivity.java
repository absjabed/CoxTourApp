package com.jabed_parvez.coxtour;

import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    TextView cityField, detailsField, currentTemperatureField, humidity_field, pressure_field, weatherIcon, updatedField;
    Typeface weatherFont;
    int[] drawablearray=new int[]{R.drawable.bg1,R.drawable.bg3,R.drawable.bg5,R.drawable.bg6,R.drawable.bg7};
    Timer _t;
    LinearLayout lnMain;
    public static int count=0;
    private LinearLayout mGallery;
    private int[] mImgIds;
    private LayoutInflater mInflater;
    private HorizontalScrollView horizontalScrollView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
/*        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setLogo(R.drawable.title_ixom);
        getSupportActionBar().setDisplayUseLogoEnabled(true);*/
        setContentView(R.layout.activity_main);



        //------------Weather fields
        weatherFont = Typeface.createFromAsset(getAssets(),"fonts/weathericons-regular-webfont.ttf");

        cityField = (TextView)findViewById(R.id.city_field);
        updatedField = (TextView)findViewById(R.id.updated_field);
        detailsField = (TextView)findViewById(R.id.details_field);
        currentTemperatureField = (TextView)findViewById(R.id.current_temperature_field);
        weatherIcon = (TextView)findViewById(R.id.weather_icon);
        weatherIcon.setTypeface(weatherFont);



        mInflater = LayoutInflater.from(this);
        initData();
        initView();

        horizontalScrollView = (HorizontalScrollView) findViewById(R.id.horigalary);
        final ObjectAnimator animator;

        animator = ObjectAnimator.ofInt(horizontalScrollView, "scrollX",mImgIds.length*279);
        animator.setRepeatCount(ValueAnimator.INFINITE);
        animator.setStartDelay(200);
        animator.setDuration(15000);

        animator.start();

        horizontalScrollView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                animator.setRepeatCount(0);
                animator.cancel();
                animator.setRepeatCount(ValueAnimator.INFINITE);
                animator.setStartDelay(6000);
                animator.start();
                return false;
            }
        }) ;

        mGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mGallery.getChildAt(1);

            }
        });



        GridView gridview = (GridView) findViewById(R.id.gridview);

      /*  gridview.setAdapter(new FruitAdapter(
                new FruitItem(R.drawable.hospital),
                new FruitItem(R.drawable.transport),
                new FruitItem(R.drawable.restaurant),
                new FruitItem(R.drawable.lifestyle),
                new FruitItem(R.drawable.yourlocation),
                 new FruitItem(R.drawable.beachactivities),
                new FruitItem(R.drawable.tidalinfo),
                new FruitItem(R.drawable.tourtravels),
                new FruitItem(R.drawable.saftysecurity),
                new FruitItem(R.drawable.complainandsugg),
                new FruitItem(R.drawable.helpline),
                new FruitItem(R.drawable.hotelmotel)));*/
        gridview.setAdapter(new FruitAdapter(
                new FruitItem("Hospital",R.drawable.hospital),
                new FruitItem("Transport",R.drawable.transport),
                new FruitItem("Restaurant",R.drawable.restaurant),
                new FruitItem("Lifestyle",R.drawable.lifestyle),
                new FruitItem("Location info",R.drawable.yourlocation),
                 new FruitItem("Beach Activities",R.drawable.beachactivities),
                new FruitItem("Tidal info",R.drawable.tidalinfo),
                new FruitItem("Tour-Travels",R.drawable.tourtravels),
                new FruitItem("Safety-Security",R.drawable.saftysecurity),
                new FruitItem("Opinion",R.drawable.complainandsugg),
                new FruitItem("Helpline",R.drawable.helpline),
                new FruitItem("Hotel-Motel",R.drawable.hotelmotel)));
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        gridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
                switch(position) {
                    case 0:
                        Intent intent = new Intent(MainActivity.this,HospitalActivity.class);
                        startActivity(intent);
                        break;
                    case 1:
                        Intent intent1 = new Intent(MainActivity.this,TransportActivity.class);
                        startActivity(intent1);
                        break;
                    case 2:
                        Intent webviewIntent = new Intent(MainActivity.this,BlankActivity.class);
                        startActivity(webviewIntent);
//                        Intent intent2 = new Intent(MainActivity.this,Hotelmotel2Activity.class);
//                        startActivity(intent2);
                        break;
                    case 3:
                        Intent intent3 = new Intent(MainActivity.this,BlankActivity.class);
                        startActivity(intent3);
                        break;
                    case 4:
                        Intent intent4 = new Intent(MainActivity.this,LocationActivity.class);
                        startActivity(intent4);
                        break;
                    case 5:
                        Intent intent5 = new Intent(MainActivity.this,EventfinalActivity.class);
                        startActivity(intent5);
                        break;
                    case 6:
                        Intent intent6 = new Intent(MainActivity.this,TideActivity.class);
                        startActivity(intent6);
                        break;
                    case 7:
                        Intent intent7 = new Intent(MainActivity.this,ToakActivity.class);
                        startActivity(intent7);
                        break;
                    case 8:
                    Intent intent8 = new Intent(MainActivity.this,Hotelmotel2Activity.class);
                    startActivity(intent8);
                        break;
                    case 9:
                        Intent intent9 = new Intent(MainActivity.this,ComplainActivity.class);
                        startActivity(intent9);
                        break;
                    case 10:
                        Intent intent10 = new Intent(MainActivity.this,HelpLineActivity.class);
                        startActivity(intent10);
                        break;
                    case 11:
                        Intent intent11 = new Intent(MainActivity.this,Hotellist.class);
                        startActivity(intent11);
                        break;

                }
                }
             });

        TextView textView = (TextView)findViewById(R.id.tx);

        textView.setSelected(true);
        textView.setEllipsize(TextUtils.TruncateAt.MARQUEE);
        textView.setSingleLine(true);
       /* FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                *//*Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();*//*
            }
        });*/

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);



        lnMain = (LinearLayout) findViewById(R.id.linlayout);
        _t = new Timer();
        _t.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {

                runOnUiThread(new Runnable() // run on ui thread
                {
                    public void run() {
                        if (count < drawablearray.length) {

                            lnMain.setBackgroundResource(drawablearray[count]);
                            count = (count + 1) % drawablearray.length;
                        }
                    }
                });
            }
        }, 5000, 5000);


        Function.placeIdTask asyncTask =new Function.placeIdTask(new Function.AsyncResponse() {
            public void processFinish(String weather_city, String weather_description, String weather_temperature, String weather_humidity, String weather_pressure, String weather_updatedOn, String weather_iconText, String sun_rise) {

                cityField.setText(weather_city);
                updatedField.setText(weather_updatedOn);
                detailsField.setText(weather_description);
                currentTemperatureField.setText(weather_temperature);
                weatherIcon.setText(Html.fromHtml(weather_iconText));

            }
        });
        asyncTask.execute("21.453239", "91.979767"); //  asyncTask.execute("Latitude", "Longitude")


    }



    public class OurThreadsend extends Thread {

        boolean flag = true;

        public OurThreadsend() {


            super();


        }


        @Override
        public void run() {

            while(flag==true) {

                try {
                    sleep(8000);
                } catch (InterruptedException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                flag = false;
            }
/*
            horizontalScrollView = (HorizontalScrollView) findViewById(R.id.horigalary);
            ObjectAnimator animator;
            if(flag) {

                animator = ObjectAnimator.ofInt(horizontalScrollView, "scrollX", 1200);
                animator.setStartDelay(800);
                animator.setDuration(10000);
                flag = false;
                animator.start();
            }
            else
            {
                animator = ObjectAnimator.ofInt(horizontalScrollView, "scrollX", 0);
                animator.setStartDelay(800);
                animator.setDuration(10000);
                flag = true;
                animator.start();
                //  animator.setRepeatCount(ValueAnimator.INFINITE);


            }*/


        }

        //--------------StopThread method--------------------------


    }

    private void initData()
    {
        mImgIds = new int[] { R.drawable.hosa, R.drawable.hosb, R.drawable.hosc,R.drawable.hosd,R.drawable.hosa, R.drawable.hosb, R.drawable.hosc,R.drawable.hosd
        };
    }

    private void initView()
    {
        mGallery = (LinearLayout) findViewById(R.id.id_gallery);

        for (int i = 0; i < mImgIds.length; i++)
        {

            View view = mInflater.inflate(R.layout.activity_gallery_item,
                    mGallery, false);
            ImageView img = (ImageView) view.findViewById(R.id.id_index_gallery_item_image);

            img.setImageResource(mImgIds[i]);
            final TextView txt = (TextView) view
                    .findViewById(R.id.id_index_gallery_item_text);
            txt.setText("info "+i);
            mGallery.addView(view);
            img.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String temp = txt.getText().toString();
                    if(temp.equals("info 1"))
                    {

                        //ad touch event
                        //           Intent intent = new Intent(DemoActivity.this,GoHotelDemo.class);
                        //         startActivity(intent);
                        //    Intent webviewIntent = new Intent(DemoActivity.this,GoHotelDemo.class);
                        //   webviewIntent.putExtra("URL","http://m.booking.com/searchresults.en-gb.html?label=gen173nr-1DCAEoggJCAlhYSDNiBW5vcmVmaBSIAQGYAS64AQjIAQzYAQPoAQGoAgM&sid=41dd855bece92a95de307532c7c5a6b0&sb=1&src=index&src_elem=sb&error_url=http%3A%2F%2Fwww.booking.com%2Findex.en-gb.html%3Flabel%3Dgen173nr-1DCAEoggJCAlhYSDNiBW5vcmVmaBSIAQGYAS64AQjIAQzYAQPoAQGoAgM%3Bsid%3D41dd855bece92a95de307532c7c5a6b0%3Bsb_price_type%3Dtotal%26%3B&ss=Cox%27s+Bazar%2C+Bangladesh&checkin_monthday=&checkin_month=&checkin_year=&checkout_monthday=&checkout_month=&checkout_year=&room1=A%2CA&no_rooms=1&group_adults=2&group_children=0&ss_raw=cox&ac_position=0&ac_langcode=en&dest_id=211349&dest_type=city&search_pageview_id=a2933ffbd95000e9&search_selected=true&search_pageview_id=a2933ffbd95000e9&ac_suggestion_list_length=5&ac_suggestion_theme_list_length=0");
//                        Intent intent = new Intent(DemoActivity.this,Hotellist.class);
//
//                        startActivity(intent);


                    }

                    Toast.makeText(getApplicationContext(), "" + txt.getText().toString(),
                            Toast.LENGTH_SHORT).show();


                }
            });
        }
    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_event) {
            Intent intent = new Intent(this,Event.class);
            startActivity(intent);

        } else if (id == R.id.nav_ain) {

            Intent intent1 = new Intent(MainActivity.this,AinsriActivity.class);
            startActivity(intent1);
        } else if (id == R.id.nav_hotel) {

            Intent intent = new Intent(this,Hotelmotel2Activity.class);
            startActivity(intent);

        } else if (id == R.id.nav_toak) {
            Intent intent = new Intent(MainActivity.this,ToakActivity.class);
            startActivity(intent);

        }
        else if (id == R.id.nav_lifestyle) {
            Intent intent5 = new Intent(MainActivity.this,Example32Activity.class);
            startActivity(intent5);
        }
        else if (id == R.id.nav_complain) {
            Intent intent9 = new Intent(MainActivity.this,ComplainActivity.class);
            startActivity(intent9);
        }
         else if (id == R.id.nav_helpline) {
            Intent intent10 = new Intent(MainActivity.this,HelpLineActivity.class);
            startActivity(intent10);
        }
        else if (id == R.id.nav_share) {

        }else if (id == R.id.nav_about) {
            Intent intent12 = new Intent(MainActivity.this,Activity_About.class);
            startActivity(intent12);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
